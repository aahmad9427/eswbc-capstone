/*
*********************************************************************************************************
*                                            EXAMPLE CODE
*
*               This file is provided as an example on how to use Micrium products.
*
*               Please feel free to use any application code labeled as 'EXAMPLE CODE' in
*               your application products.  Example code may be used as is, in whole or in
*               part, or may be used as a reference only. This file can be modified as
*               required to meet the end-product requirements.
*
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*
*               You can find our product's user manual, API reference, release notes and
*               more information at https://doc.micrium.com.
*               You can contact us at www.micrium.com.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                            EXAMPLE CODE
*
*                                         STM32F746G-DISCO
*                                         Evaluation Board
*
* Filename      : app_main.c
* Version       : V1.00
* Programmer(s) : FF
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/

#include  "stdarg.h"
#include  "stdio.h"
#include  "stm32f7xx_hal.h"

#include  "cpu.h"
#include  "lib_math.h"
#include  "lib_mem.h"
#include  "os.h"
#include  "os_app_hooks.h"

#include "pushbutton.h"
#include "adc.h"
#include "alarm.h"
#include  "app_cfg.h"
#include  "bsp.h"
#include  "bsp_led.h"
#include  "bsp_clock.h"
#include  "bsp_pb.h"
#include  "bsp_test.h"
#include  "GUI.h"
#include "GUIDEMO_API.h"

typedef struct {
  BOARD_LED_ID led_id;
  int frequency;
} LED_T;

typedef struct {
  OS_FLAG_GRP * pend_flag;
  int counter;
  uint8_t sw_num;
} SW_T;

typedef struct {
  uint8_t seconds;
  uint8_t minutes;
  uint8_t hours;
} TIME_T;

// *****************************************************************
// Define storage for each Task Control Block (TCB) and task stacks
// *****************************************************************
static  OS_TCB   AppTaskStartup_TCB;
static  CPU_STK  AppTaskStartup_Stk[APP_CFG_STARTUP_TASK_STK_SIZE];

static  OS_TCB   AppTaskGUI_TCB;
static  CPU_STK  AppTaskGUI_Stk[APP_CFG_TASK_GUI_STK_SIZE];

static  OS_TCB   AppTaskBlink_LED1_TCB;
static  CPU_STK  AppTaskBlink_LED1_Stk[APP_CFG_BLINK_LED1_TASK_STK_SIZE];

static OS_TCB  AppTaskDebounce_TCB;
static CPU_STK AppTaskDebounce_Stk[APP_CFG_BLINK_DEBOUNCE_TASK_STK_SIZE];

static OS_TCB  AppTaskADC_TCB;
static CPU_STK AppTaskADC_Stk[APP_CFG_ADC_TASK_STK_SIZE];

static OS_TCB  AppTaskAlarm_TCB;
static CPU_STK AppTaskAlarm_Stk[APP_CFG_ALARM_TASK_STK_SIZE];

static OS_TCB AppTaskTime_TCB;
static CPU_STK AppTaskTime_Stk[APP_CFG_TIME_TASK_STK_SIZE];

static OS_MUTEX gp_led_mutex;

// *****************************************************************
// Flash LED1 at 1 Hz (500ms on / 500ms off)
// *****************************************************************
static void led1_task(void * p_arg)
{
    // Cool code bro

    OS_ERR  err;
    LED_T * led = (LED_T *) p_arg;
    // Task main loop
    for (;;)
    {
        OSMutexPend(&gp_led_mutex, 0, OS_OPT_PEND_BLOCKING, NULL, &err);
        my_assert(OS_ERR_NONE == err);
        BSP_LED_Toggle(led->led_id);
        OSMutexPost(&gp_led_mutex, OS_OPT_POST_NONE, &err);
        OSTimeDlyHMSM(0,0,0,led->frequency,0, &err);
    }
}

// *****************************************************************
// Dive timer Task
// *****************************************************************
static void time_task(void * p_arg)
{
  OS_ERR err;

  TIME_T current_time = {0,0,0};
  uint32_t depth_status;
  char p_str[8];

  for (;;)
  {
    depth_status = OSFlagPend(&g_depth_flags, DEPTH_SURFACE, 0,
                  OS_OPT_PEND_FLAG_SET_ANY | OS_OPT_PEND_NON_BLOCKING, NULL, &err);
    my_assert(OS_ERR_NONE == err || OS_ERR_PEND_WOULD_BLOCK == err);
    OSTimeDlyHMSM(0,0,1,0,0,&err);
    if (depth_status!=DEPTH_SURFACE) {
      my_assert(OS_ERR_NONE == err);
      if (current_time.seconds == 59) {
        current_time.seconds = 0;
        if (current_time.minutes == 59) {
          current_time.minutes = 0;
          current_time.hours++;
        } else {
          current_time.minutes++;
        }
      } else {
        current_time.seconds++;
      }
    }
    sprintf(p_str, "TIME: %1u:%02u:%02u", current_time.hours, current_time.minutes, current_time.seconds);
    GUIDEMO_API_writeLine(6, p_str);
  }

}


// *****************************************************************
// Startup Task
// *****************************************************************

static void startup_task(void * p_arg)
{
  OS_ERR err;

  //BSP init
  BSP_Init();

#if OS_CFG_STAT_TASK_EN > 0u
    // Compute CPU capacity with no other task running
    OSStatTaskCPUUsageInit(&err);
    my_assert(OS_ERR_NONE == err);
    OSStatReset(&err);
    my_assert(OS_ERR_NONE == err);
#endif

#ifdef CPU_CFG_INT_DIS_MEAS_EN
    CPU_IntDisMeasMaxCurReset();
#endif

    LED_T led1 = {LED1, 161};

    OSMutexCreate(&gp_led_mutex, "LED Mutex", &err);
    my_assert(OS_ERR_NONE == err);

    /* Initialize Tasks */
    adc_task_init();
    debounce_task_init();

    char p_str[15];

     // Create the GUI task
    OSTaskCreate(&AppTaskGUI_TCB, "uC/GUI Task", (OS_TASK_PTR ) GUI_DemoTask,
                 0, APP_CFG_TASK_GUI_PRIO,
                 &AppTaskGUI_Stk[0], (APP_CFG_TASK_GUI_STK_SIZE / 10u),
                  APP_CFG_TASK_GUI_STK_SIZE, 0u, 0u, 0,
                  (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
    my_assert(OS_ERR_NONE == err);

    // TODO: Create task to blink LED1
    OSTaskCreate(&AppTaskBlink_LED1_TCB, "uC/Blink LED1 Task", (OS_TASK_PTR) led1_task, (void *) &led1, APP_CFG_BLINK_LED1_TASK_PRIO,
                 &AppTaskBlink_LED1_Stk[0], (APP_CFG_BLINK_LED1_TASK_STK_SIZE / 10u),
                 APP_CFG_BLINK_LED1_TASK_STK_SIZE, 0u, 0u, 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
    my_assert(OS_ERR_NONE == err);

    // Button Debounce Task - will increment the SW1 and SW2 semaphores as necessary
    OSTaskCreate(&AppTaskDebounce_TCB, "uC/Debounce Task", (OS_TASK_PTR) debounce_task,
                 0, APP_CFG_BLINK_DEBOUNCE_TASK_PRIO, &AppTaskDebounce_Stk[0], (APP_CFG_BLINK_DEBOUNCE_TASK_STK_SIZE/10u),
                 APP_CFG_BLINK_DEBOUNCE_TASK_STK_SIZE, 0u, 0, 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );
    my_assert(OS_ERR_NONE == err);

    // Keeps track of ADC values by converting values every 125ms
    OSTaskCreate(&AppTaskADC_TCB, "uC/ADC Task", (OS_TASK_PTR) adc_task,
                 0, APP_CFG_ADC_TASK_PRIO, &AppTaskADC_Stk[0], (APP_CFG_ADC_TASK_STK_SIZE/10u),
                 APP_CFG_ADC_TASK_STK_SIZE, 0u, 0, 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );
    my_assert(OS_ERR_NONE == err);

    // Alarm task to print current status on BG
    OSTaskCreate(&AppTaskAlarm_TCB, "uC/SW Alarm Task", (OS_TASK_PTR) alarm_task,
                 0, APP_CFG_ALARM_TASK_PRIO, &AppTaskAlarm_Stk[0], (APP_CFG_ALARM_TASK_STK_SIZE/10u),
                 APP_CFG_ALARM_TASK_STK_SIZE, 0u, 0, 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );
    my_assert(OS_ERR_NONE == err);

    // Time task to print current time on LCD
    OSTaskCreate(&AppTaskTime_TCB, "uC/SW Time Task", (OS_TASK_PTR) time_task,
                 0, APP_CFG_TIME_TASK_PRIO, &AppTaskTime_Stk[0], (APP_CFG_TIME_TASK_STK_SIZE/10u),
                 APP_CFG_TIME_TASK_STK_SIZE, 0u, 0, 0, (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err );
    my_assert(OS_ERR_NONE == err);

    sprintf(p_str, "ESBC Capstone");
    GUIDEMO_API_writeLine(1, p_str);
    sprintf(p_str, "TIME: 0:00:00");
    GUIDEMO_API_writeLine(6, p_str);

    OSTaskSuspend(&AppTaskStartup_TCB, &err);

    while(1) { }

}


// *****************************************************************
int main(void)
{
    OS_ERR   err;

    HAL_Init();
    BSP_SystemClkCfg();   // Init. system clock frequency to 200MHz
    CPU_Init();           // Initialize the uC/CPU services
    Mem_Init();           // Initialize Memory Managment Module
    Math_Init();          // Initialize Mathematical Module
    CPU_IntDis();         // Disable all Interrupts.

    // TODO: Init uC/OS-III.
    OSInit(&err);
    // Create the startup task
     OSTaskCreate(&AppTaskStartup_TCB, "uC/Startup Task", (OS_TASK_PTR ) startup_task,
               0, APP_CFG_STARTUP_TASK_PRIO,
               &AppTaskStartup_Stk[0], (APP_CFG_STARTUP_TASK_STK_SIZE / 10u),
                APP_CFG_STARTUP_TASK_STK_SIZE, 0u, 0u, 0,
                (OS_OPT_TASK_STK_CHK | OS_OPT_TASK_STK_CLR), &err);
    my_assert(OS_ERR_NONE == err);

    // TODO: Start multitasking (i.e. give control to uC/OS-III)
     OSStart(&err);
     my_assert(OS_ERR_NONE == err);
    // while(1){};

    // Should never get here
    my_assert(0);
}
