/** \file alarm.c
*
* @brief Alarm Manager
*
* @par
* COPYRIGHT NOTICE: (c) 2017 Barr Group, LLC.
* All rights reserved.
*/

#include <stdint.h>
#include <stdio.h>

#include "project.h"
#include "os.h"
#include "alarm.h"
#include "adc.h"
#include  "GUIDEMO_API.h"

/*!
*
* @brief Alarm Task
*/
void alarm_task(void * p_arg)
{

  OS_ERR err;
  OS_FLAGS flags_rx;
  // TODO: This task should pend on events from adc_task(),
  //       and then update the LCD background color accordingly.
  for (;;){
    flags_rx = OSFlagPend(&g_alarm_flags, 
                          ALARMS, 
                          0, 
                          (OS_OPT_PEND_FLAG_SET_ANY | OS_OPT_PEND_FLAG_CONSUME), 
                          NULL, 
                          &err);
    my_assert(OS_ERR_NONE == err);

    if (flags_rx & ALARM_HIGH)
    {
      GUIDEMO_SetColorBG(BG_COLOR_RED);
      GUIDEMO_API_writeLine(7U, "ALARM: HIGH (Insufficent Air to Surface)");
      // GUIDEMO_API_writeLine(7U, "ALARM: HIGH (You're DOOOOOOOMMMMED)");
    } 
    else if (flags_rx & ALARM_MEDIUM)
    {
      GUIDEMO_SetColorBG(BG_COLOR_ORANGE);
      GUIDEMO_API_writeLine(7U, "ALARM: MEDIUM (Dangerous Ascent Rate)");
    } 
    else if (flags_rx & ALARM_LOW)
    {
      GUIDEMO_SetColorBG(BG_COLOR_YELLOW);
      GUIDEMO_API_writeLine(7U, "ALARM: LOW (Dangerous Depth)");
    } 
    else if (flags_rx & ALARM_NONE)
    {
      GUIDEMO_SetColorBG(BG_COLOR_GREEN);
      GUIDEMO_API_writeLine(7U, "ALARM: NONE");
    }

  }
}
