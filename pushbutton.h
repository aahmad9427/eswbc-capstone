/** \file pushbutton.h
*
* @brief Button Debouncer
*
* @par
* COPYRIGHT NOTICE: (C) 2017 Barr Group
* All rights reserved.
*/

#ifndef _PUSHBUTTON_H
#define _PUSHBUTTON_H

#include "os.h"
#include "app_cfg.h"

#define BIT(n)  (1UL << (n))

#define BTN_UNIT    BIT(0UL)
#define BTN_AIR     BIT(1UL)
#define BTNS        (BTN_UNIT | BTN_AIR)

extern OS_FLAG_GRP g_BUTTONS_EVTS;

void  debounce_task_init(void);
void  debounce_task(void * p_arg);

#endif /* _PUSHBUTTON_H */
