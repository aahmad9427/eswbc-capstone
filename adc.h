/** \file adc.h
*
* @brief ADC Task interface
*
* @par
* COPYRIGHT NOTICE: (c) 2017 Barr Group, LLC.
* All rights reserved.
*/
#ifndef _ADC_H
#define _ADC_H

#define BIT(n)  (1UL << (n))

#define DEPTH_SURFACE    BIT(0UL)

#define MAX_DEPTH_M (40)
#define MAX_ASCEND_RATE_M_MIN (15)

void adc_task_init(void);
extern void     adc_task (void * p_arg);
extern void     ADC_IRQHandler(void);

extern OS_FLAG_GRP g_alarm_flags;
extern OS_FLAG_GRP g_depth_flags;

static void pot_init(void);
#endif /* _ADC_H */
